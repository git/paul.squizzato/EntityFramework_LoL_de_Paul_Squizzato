# Description de l'architecture :

L'architecture de l'application se découpe en quelques majeures parties :
- La partie Model, elle contient toutes les classes des différents éléments du jeu League of Legends qui nous intéressent.
- La partie ModelEF, elle s'occupe de la persistance en base de données avec des versions entités des classes du Model qui contiennent les informations que l'on souhaite sauvegarder.
- La partie TrucAuMilieu. Ce truc, au milieu des parties Model et ModelEF, permet la traduction d'une classe du Model, telle que Champion, en une classe du ModelEF, telle que ChampionEntity. Pour faire simple, elle permet de passer des classes utilisées par l'application aux classes utilisées par la base de données.
- La partie ApiDePaul, qui contient elle même différentes parties :
    - DTO représente les classes qui vont être utilisées par l'Api, ce sont alors des réprésentations des classes du Model.
    - Controllers contient les requêtes REST que l'Api va appeler, afin d'effectuer des requêtes CRUD sur les données des classes DTO.
    - Conversion comporte des classes qui vont traduire les classes du Model en classes DTO, afin de les rendre exploitables à l'Api.