﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
namespace LibEntityFramework
{
    public class ChampionContext : DbContext
    {
        public ChampionContext() { }
        public ChampionContext(DbContextOptions<ChampionContext> options) : base(options) { }
        public DbSet<ChampionEntity> Champs { get; set; }
        public DbSet<SkinEntity> Skins { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (!options.IsConfigured)
            { 
                options.UseSqlite("DataSource=laBdd.db");
            }
        }
    }
}
