﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibEntityFramework
{
    public class SkinEntity
    {
        [Key]
        public string Name { get; set; }
        public string Champion { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public string Image { get; set; }
        public float Price { get; set; }
    }
}
