﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibEntityFramework
{
    public class ChampionEntity
    {
        //j'ai retiré l'Id, je me suis dit que ça ferait plus de sens de ne pas toucher au model, car ce code n'est pas le mien.
        [Key]
        public string Name { get; set; }
        public string Bio { get; set; }
        public string Class {  get; set; }
        public string Icon { get; set; }
        public string Image { get; set; }
        /*public List<Tuple<string, int>> Characteristics { get; set; }  faudra les faire en base eux aussi
        public List<SkillDto> Skills { get; set; }*/
        /*public ChampionEntity(string nameid, string bio = "", string Class = "", string icon = "", string image = "")
        {
            NameId = nameid;
            Bio = bio;
            this.Class = Class;
            Icon = icon;
            Image = image;
        }*/
    }
}
