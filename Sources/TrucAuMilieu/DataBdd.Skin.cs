﻿using LibEntityFramework;
using Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrucAuMilieu
{
    public partial class DataBdd
    {
        public ChampionContext ct = new ChampionContext();
        public class SkinManager : ISkinsManager
        {
            private readonly DataBdd parent;
            public SkinManager(DataBdd parent)
                => this.parent = parent;
            public async Task<Skin?> AddItem(Skin? item)
            {
                await parent.ct.AddAsync(item.SkinToEf());
                await parent.contextCh.SaveChangesAsync();
                Debug.WriteLine("skin ajouté");
                return item;
            }

            public Task<bool> DeleteItem(Skin? item)
            {
                throw new NotImplementedException();
            }

            public async Task<IEnumerable<Skin?>> GetItems(int index, int count, string? orderingPropertyName = null, bool descending = false)
            //requete sur les champions avec des arguments (genre la pagination est possible, et y'a le descending et tout)
            {
                if (orderingPropertyName != null)
                {
                    if (descending)
                    {
                        return await Task.FromResult(parent.ct.Skins.OrderByDescending(s => typeof(SkinEntity).GetProperty(orderingPropertyName)).Skip(index * count).Take(count).Select(sk => sk.EfToSkin()));
                    }
                    else
                    {
                        var pap = await Task.FromResult(parent.ct.Skins.OrderBy(c => typeof(SkinEntity).GetProperty(orderingPropertyName)).Skip(index * count).Take(count).Select(sk => sk.EfToSkin()));
                        return pap;
                    }
                }
                else
                {
                    var pap2 = await Task.FromResult(parent.ct.Skins.Skip(index * count).Take(count).Select(sk => sk.EfToSkin()));
                    return pap2;
                }
            }

            public Task<IEnumerable<Skin?>> GetItemsByChampion(Champion? champion, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                throw new NotImplementedException();
            }

            public Task<IEnumerable<Skin?>> GetItemsByName(string substring, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                throw new NotImplementedException();
            }
            public Task<int> GetNbItems() => Task.FromResult(parent.ct.Skins.Count());

            public Task<int> GetNbItemsByChampion(Champion? champion)
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItemsByName(string substring)
            {
                throw new NotImplementedException();
            }

            public Task<Skin?> UpdateItem(Skin? oldItem, Skin? newItem)
            {
                throw new NotImplementedException();
            }
        }
    }
}
