﻿using LibEntityFramework;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrucAuMilieu
{
    public static class ChampionConvert
    {
        public static ChampionEntity ChampToEf(this Champion c)
        {
            /*foreach(var sk in c.Skins)
            {
                faudrait faire avec les autres trucs
            }*/
            return new ChampionEntity()
            {
                Name=c.Name,
                Bio=c.Bio,
                Class=c.Class.ToString(),
                Icon=c.Icon,
                Image=c.Image.ToString()
            };
        }
        public static Champion EfToChamp(this ChampionEntity c)
        {
            Champion champ = new Champion(c.Name);
            var classes = Enum.GetNames(typeof(ChampionClass));
            if (classes.ToList().Contains(c.Class))
            {
                champ.Class = (ChampionClass)Enum.Parse(typeof(ChampionClass), c.Class);
            }
            else
            {
                champ.Class = 0;
            }
            champ.Icon = c.Icon;
            champ.Image = new LargeImage(c.Image);
            champ.Bio = c.Bio;
            /*
            foreach (var s in c.Skills)
            {
                champ.AddSkill(s.DtoToSkill());
            }
            champ.AddCharacteristics(c.Characteristics.ToArray());
            */
            return champ;

        }
    }
}
