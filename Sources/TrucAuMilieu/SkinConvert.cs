﻿using LibEntityFramework;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrucAuMilieu
{
    public static class SkinConvert
    {
        public static SkinEntity SkinToEf(this Skin s)
        {
            return new SkinEntity()
            {
                Name = s.Name,
                Champion = s.Champion.Name,
                Description = s.Description,
                Icon = s.Icon,
                Image = s.Image.ToString(),
                Price = s.Price
            };
        }
        public static Skin EfToSkin(this SkinEntity s)
        {
            var dataMgr = new DataBdd();
            Champion c = dataMgr.ChampionsMgr.GetItemsByName(s.Champion, 0, 1).Result.First();
            return new Skin(s.Name,c,s.Price,s.Icon,s.Image,s.Description);
        }
    }
}