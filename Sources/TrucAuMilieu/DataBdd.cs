﻿using LibEntityFramework;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrucAuMilieu
{
    public partial class DataBdd : IDataManager
    {
        public DataBdd()
        {
            ChampionsMgr = new ChampionsManager(this);
            SkinsMgr = new SkinManager(this);
        }
        public IChampionsManager ChampionsMgr { get; }

        public ISkinsManager? SkinsMgr { get; }

        public IRunesManager? RunesMgr { get; }

        public IRunePagesManager? RunePagesMgr { get; }
    }
}
