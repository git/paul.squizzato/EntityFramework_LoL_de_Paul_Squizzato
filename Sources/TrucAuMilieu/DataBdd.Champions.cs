﻿using LibEntityFramework;
using Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrucAuMilieu
{
    public partial class DataBdd
    {
        public ChampionContext contextCh = new ChampionContext();
        public class ChampionsManager : IChampionsManager
        {
            private readonly DataBdd parent;
            public ChampionsManager(DataBdd parent)
                => this.parent = parent;
            public async Task<Champion?> AddItem (Champion? item)
            //ajoute un champion à la base
            {
                await parent.contextCh.AddAsync(item.ChampToEf());
                await parent.contextCh.SaveChangesAsync();
                Debug.WriteLine("champion ajouté");
                return item;
            }

            public async Task<bool> DeleteItem(Champion? item)
            //supprimer un champion? je crois? et renvoie true si il est supprimé? à tester
            {
                if (parent.contextCh.Champs.Remove(item.ChampToEf()).Entity==item.ChampToEf())
                {
                    await parent.contextCh.SaveChangesAsync();
                    return true;
                }
                return false;
            }

            public async Task<IEnumerable<Champion?>> GetItems(int index, int count, string? orderingPropertyName = null, bool descending = false)
            //requete sur les champions avec des arguments (genre la pagination est possible, et y'a le descending et tout)
            {
                if (orderingPropertyName != null)
                {
                    if (descending)
                    {
                        return await Task.FromResult(parent.contextCh.Champs.OrderByDescending(c => typeof(ChampionEntity).GetProperty(orderingPropertyName)).Skip(index * count).Take(count).Select(ce => ce.EfToChamp()));
                    }
                    else
                    {
                        var pap = await Task.FromResult(parent.contextCh.Champs.OrderBy(c => typeof(ChampionEntity).GetProperty(orderingPropertyName)).Skip(index * count).Take(count).Select(ce => ce.EfToChamp()));
                        return pap;
                    }
                }
                else
                {
                    var pap2= await Task.FromResult(parent.contextCh.Champs.Skip(index*count).Take(count).Select(ce => ce.EfToChamp()));
                    return pap2;
                }
            }

            public Task<IEnumerable<Champion?>> GetItemsByCharacteristic(string charName, int index, int count, string? orderingPropertyName = null, bool descending = false)
            //pas fait
            {
                throw new NotImplementedException();
            }

            public Task<IEnumerable<Champion?>> GetItemsByClass(ChampionClass championClass, int index, int count, string? orderingPropertyName = null, bool descending = false)
            //pas fait
            {
                throw new NotImplementedException();
            }

            public async Task<IEnumerable<Champion?>> GetItemsByName(string substring, int index, int count, string? orderingPropertyName = null, bool descending = false)
            //requete des champions dont le nom contient substring
            {
                var i = from c in parent.contextCh.Champs where c.Name.Contains(substring) select c;
                if (orderingPropertyName != null)
                {
                    if (descending)
                    {
                        return await Task.FromResult(i.OrderByDescending(c => typeof(ChampionEntity).GetProperty(orderingPropertyName)).Skip(index * count).Take(count).Select(ce => ce.EfToChamp()));
                    }
                    else
                    {
                        var pap = await Task.FromResult(i.OrderBy(c => typeof(ChampionEntity).GetProperty(orderingPropertyName)).Skip(index * count).Take(count).Select(ce => ce.EfToChamp()));
                        return pap;
                    }
                }
                else
                {
                    var pap2 = await Task.FromResult(i.Skip(index * count).Take(count).Select(ce => ce.EfToChamp()));
                    return pap2;
                }
                
            }

            public Task<IEnumerable<Champion?>> GetItemsByRunePage(RunePage? runePage, int index, int count, string? orderingPropertyName = null, bool descending = false)
            //pas fait
            {
                throw new NotImplementedException();
            }

            public Task<IEnumerable<Champion?>> GetItemsBySkill(Skill? skill, int index, int count, string? orderingPropertyName = null, bool descending = false)
            //pas fait
            {
                throw new NotImplementedException();
            }

            public Task<IEnumerable<Champion?>> GetItemsBySkill(string skill, int index, int count, string? orderingPropertyName = null, bool descending = false)
            //pas fait
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItems() => Task.FromResult(parent.contextCh.Champs.Count());
            //donne le nombre de champions dans la base

            public Task<int> GetNbItemsByCharacteristic(string charName)
            //pas fait
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItemsByClass(ChampionClass championClass)
            //pas fait
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItemsByName(string substring)
            //pas fait
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItemsByRunePage(RunePage? runePage)
            //pas fait
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItemsBySkill(Skill? skill)
            //pas fait
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItemsBySkill(string skill)
            //pas fait
            {
                throw new NotImplementedException();
            }

            public Task<Champion?> UpdateItem(Champion? oldItem, Champion? newItem)
            //met à jour un champion (pas fait)
            {
                throw new NotImplementedException();
            }
        }
    }
}
