using LibEntityFramework;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model;
using TrucAuMilieu;

namespace TestsTresImportants
{
    public class UTestsChampion
    {
        [Fact]
        public void Test()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<ChampionContext>()
                .UseSqlite(connection)
                .Options;

            using (var context = new ChampionContext(options))
            {
                context.Database.EnsureCreated();

                Champion c1 = new Champion("Joe");
                Champion c2 = new Champion("Billy");
                Champion c3 = new Champion("Robert");

                context.Champs.Add(c1.ChampToEf());
                context.Champs.Add(c2.ChampToEf());
                context.Champs.Add(c3.ChampToEf());
                context.SaveChanges();
            }
            using(var context= new ChampionContext(options))
            {
                context.Database.EnsureCreated();
                Assert.Equal(3, context.Champs.Count());
                Assert.Equal("Joe", context.Champs.First().Name);
            }

        }
    }
}