﻿using ApiDePaul.DTO;
using Microsoft.AspNetCore.Mvc;
using Model;
using StubLib;
using System.Collections.Generic;

namespace ApiDePaul.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SkinController : ControllerBase
    {
        IDataManager donnees;// = new StubData();

        private readonly ILogger<SkinController> _logger;

        public SkinController(ILogger<SkinController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<SkinDto>> GetSkins()
        {
            IEnumerable<Skin?> lsk = await donnees.SkinsMgr.GetItems(0, donnees.SkinsMgr.GetNbItems().Result);

            List<SkinDto> skins = new List<SkinDto>();

            lsk.ToList().ForEach(s => skins.Add(s.SkinToDto()));
            return Ok(skins);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<SkinDto>> GetSkinId(int id)
        {
            IEnumerable<Skin?> lsk = await donnees.SkinsMgr.GetItems(id, 1);

            if (id >= 0 && id < donnees.SkinsMgr.GetNbItems().Result)
            {
                return Ok(lsk.First().SkinToDto());
            }
            return BadRequest();
        }


        [HttpGet("Page/{numpage}/{nbparpage}")]
        public async Task<ActionResult<PageDto<SkinDto>>> GetSkinsParPage(int numpage, int nbparpage)
        {
            PageDto<SkinDto> page = new PageDto<SkinDto>();
            page.NumeroPage = numpage;
            page.NombreObjetsParPage = nbparpage;
            page.Objets = new List<SkinDto>();
            page.TotalObjets = donnees.SkinsMgr.GetNbItems().Result;
            IEnumerable<Skin?> lcha = await donnees.SkinsMgr.GetItems(numpage,nbparpage);
            lcha.ToList().ForEach(c => page.Objets.Add(c.SkinToDto()));
            return Ok(page);
        }
    }
}
