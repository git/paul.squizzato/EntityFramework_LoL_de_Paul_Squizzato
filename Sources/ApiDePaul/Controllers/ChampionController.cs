﻿using ApiDePaul.DTO;
using Microsoft.AspNetCore.Mvc;
using Model;
using StubLib;
using System.Collections.Generic;
using System.Diagnostics;

namespace ApiDePaul.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ChampionController : ControllerBase
    {
        IDataManager donnees = new StubData();

        private readonly ILogger<ChampionController> _logger;

        public ChampionController(ILogger<ChampionController> logger,IDataManager datamgr)
        {
            _logger = logger;
            this.donnees = datamgr;
        }

        [HttpGet]
        public async Task<ActionResult<ChampionDto>> GetChamps()
        {
            IEnumerable<Champion?> lcha = await donnees.ChampionsMgr.GetItems(0, donnees.ChampionsMgr.GetNbItems().Result);

            List<ChampionDto> champs = new List<ChampionDto>();

            lcha.ToList().ForEach(c => champs.Add(c.ChampToDto()));
            return Ok(champs);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ChampionDto>> GetChampId(int id)
        {
            IEnumerable<Champion?> lcha = await donnees.ChampionsMgr.GetItems(id, 1);

            if (id >= 0 && id < donnees.ChampionsMgr.GetNbItems().Result)
            {
                return Ok(lcha.First().ChampToDto());
            }
            return BadRequest();
        }
        [HttpGet("nombre")]
        public async Task<ActionResult<int>> GetNombreChamp()
        {
            int nb = await donnees.ChampionsMgr.GetNbItems();
            return Ok(nb);
        }


        [HttpGet("{id}/Skins")]
        public async Task<ActionResult<SkinDto>> GetSkinsChamp(int id)
        {
            IEnumerable<Champion?> lcha = await donnees.ChampionsMgr.GetItems(id, 1);

            if (id >= 0 && id < donnees.ChampionsMgr.GetNbItems().Result)
            {
                Champion ca = lcha.First();

                IEnumerable<Skin?> lsk = await donnees.SkinsMgr.GetItemsByChampion(ca, 0, donnees.SkinsMgr.GetNbItemsByChampion(ca).Result);

                List<SkinDto> skins = new List<SkinDto>();
                lsk.ToList().ForEach(s => skins.Add(s.SkinToDto()));

                return Ok(skins);
            }
            return BadRequest();
        }

        [HttpGet("Page/{numpage}/{nbparpage}")]
        public async Task<ActionResult<PageDto<ChampionDto>>> GetChampParPage(int numpage, int nbparpage)
        {
            PageDto<ChampionDto> page = new PageDto<ChampionDto>();
            page.NumeroPage = numpage;
            page.NombreObjetsParPage = nbparpage;
            page.Objets = new List<ChampionDto>();
            page.TotalObjets = donnees.ChampionsMgr.GetNbItems().Result;
            IEnumerable<Champion?> lcha = await donnees.ChampionsMgr.GetItems(numpage,nbparpage);
            lcha.ToList().ForEach(c => page.Objets.Add(c.ChampToDto()));
            return Ok(page);
        }


        [HttpPost("Ajouter/{nom}")]
        public async Task<ActionResult> PostChampName(string nom)
        {
            Champion ca = new Champion(nom);

            await donnees.ChampionsMgr.AddItem(ca);

            return CreatedAtAction(nameof(GetChampId), new { id = donnees.ChampionsMgr.GetNbItems().Result - 1 }, ca.ChampToDto());
        }
        /*   en fait cet ajouter sert à rien, vu que la liste peut contenir un champion
        [HttpPost("AjouterUnSeul")]
        public async Task<ActionResult> PostChamp([FromBody] ChampionDto c)
        {
            Champion ca = c.DtoToChamp();
            //return Ok(await donnees.ChampionsMgr.AddItem(ca));
            await donnees.ChampionsMgr.AddItem(ca);
            return CreatedAtAction(nameof(GetChampId), new { id = donnees.ChampionsMgr.GetNbItems().Result - 1 }, ca.ChampToDto());
        }*/
        [HttpPost("Ajouter")]
        public async Task<ActionResult> PostChamps([FromBody] List<ChampionDto> lc)
        {
            foreach(ChampionDto c in lc)
            {
                await donnees.ChampionsMgr.AddItem(c.DtoToChamp());
            }
            return Ok(lc.Count());
        }




        [HttpDelete("Supprimer/{id}")]
        public async Task<IActionResult> DeleteChamp(int id)
        {
            IEnumerable<Champion?> lcha = await donnees.ChampionsMgr.GetItems(id, 1);

            if (id >= 0 && id < donnees.ChampionsMgr.GetNbItems().Result)
            {
                Champion ca = lcha.First();
                return Ok(donnees.ChampionsMgr.DeleteItem(ca));
            }
            return BadRequest();
        }

        /* pareil que pour ajouter, pas besoin de faire qu'un seul champion quand une liste suffit
        [HttpPut("{id}/ModifierUnSeul")]
        public async Task<ActionResult> PutChamp(int id, [FromBody] ChampionDto c)
        {
            IEnumerable<Champion?> oldChamp = await donnees.ChampionsMgr.GetItems(id, 1);
            Champion champion1 = oldChamp.First();
            Champion champion2 = c.DtoToChamp();
            return Ok(await donnees.ChampionsMgr.UpdateItem(champion1, champion2));
        }*/
        [HttpPut("{id}/Modifier")]
        public async Task<ActionResult> PutChamps(int id, [FromBody] List<ChampionDto> lc)
        {
            IEnumerable<Champion?> oldChamp = await donnees.ChampionsMgr.GetItems(id, lc.Count());
            for(int i=id;i<lc.Count; i++)
            {
                Champion champion1 = oldChamp.Skip(i).Take(1).First();
                Champion champion2 = lc[i].DtoToChamp();
                await donnees.ChampionsMgr.UpdateItem(champion1, champion2);
            }
            return Ok(lc.Count());
        }/*
        [HttpPut("Modifier")]
        public async Task<IActionResult> PutChamp([FromBody] ChampionDto c, [FromBody] ChampionDto cNouv)
        {
            Champion ca = c.DtoToChamp();
            Champion caNouv = cNouv.DtoToChamp();
            await donnees.ChampionsMgr.UpdateItem(ca,caNouv);
            return CreatedAtAction(nameof(GetChampId), new { id = donnees.ChampionsMgr.GetItems(0,donnees.ChampionsMgr.GetNbItems().Result).Result.ToList().IndexOf(ca) }, ca);
        }*/

    }
}
