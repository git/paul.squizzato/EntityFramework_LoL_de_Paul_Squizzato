﻿using ApiDePaul.Conversion;
using ApiDePaul.DTO;
using Microsoft.AspNetCore.Mvc;
using Model;
using StubLib;
using System.Collections.Generic;
using System.Diagnostics;

namespace ApiDePaul.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RuneController : ControllerBase
    {
        IDataManager donnees = new StubData();

        private readonly ILogger<RuneController> _logger;

        public RuneController(ILogger<RuneController> logger, IDataManager datamgr)
        {
            _logger = logger;
            //this.donnees = datamgr;
        }

        [HttpGet]
        public async Task<ActionResult<RuneDto>> GetRunes()
        {
            IEnumerable<Rune?> lrun = await donnees.RunesMgr.GetItems(0, donnees.RunesMgr.GetNbItems().Result);

            List<RuneDto> runes = new List<RuneDto>();

            lrun.ToList().ForEach(r => runes.Add(r.RuneToDto()));

            return Ok(runes);
        }
    }
}