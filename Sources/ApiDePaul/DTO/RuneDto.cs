﻿namespace ApiDePaul.DTO
{
    public class RuneDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Family { get; set; }
        public string Icon { get; set; }
        public string Image { get; set; }
    }
}
