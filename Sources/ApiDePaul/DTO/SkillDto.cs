﻿namespace ApiDePaul.DTO
{
    public class SkillDto
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
    }
}
