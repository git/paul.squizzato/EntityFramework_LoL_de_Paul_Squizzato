﻿using Model;

namespace ApiDePaul.DTO
{
    public class SkinDto
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public string Icon { get; set; }
		public string Image { get; set; }
		public float Price { get; set; }
	}
}
