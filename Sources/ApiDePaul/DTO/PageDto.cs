﻿using Model;

namespace ApiDePaul.DTO
{
    public class PageDto<T>
    {
        public List<T> Objets { get; set; }
        public int NumeroPage { get; set; }
        public int NombreObjetsParPage { get; set; }
        public int TotalObjets { get; set; }
    }
}
