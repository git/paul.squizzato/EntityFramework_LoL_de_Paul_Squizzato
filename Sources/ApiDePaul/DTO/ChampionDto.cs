﻿using Model;
using System.Collections.ObjectModel;

namespace ApiDePaul.DTO
{
    public class ChampionDto
    {
        public string Name { get; set; }
        public string Bio { get; set; }
        public string Class { get; set; }
        public string Icon { get; set; }
        public string Image { get; set; }
        public List<SkinDto> Skins { get; set; }
        public List<Tuple<string,int>> Characteristics { get; set; }
        public List<SkillDto> Skills { get; set; }
    }
}
