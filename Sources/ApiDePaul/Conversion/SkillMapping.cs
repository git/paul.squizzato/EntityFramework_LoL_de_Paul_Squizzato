﻿using ApiDePaul.DTO;
using Model;

namespace ApiDePaul.Conversion
{
    public static class SkillMapping
    {
        public static SkillDto SkillToDto(this Skill s)
        {
            SkillDto ski = new SkillDto();
            ski.Name = s.Name;
            ski.Type = s.Type.ToString();
            ski.Description = s.Description;
            return ski;
        }
        public static Skill DtoToSkill(this SkillDto ski)
        {
            var types = Enum.GetNames(typeof(SkillType)).ToList();
            SkillType type;
            if (types.Contains(ski.Type.ToString()))
            {
                type = (SkillType)Enum.Parse(typeof(SkillType), ski.Type);
            }
            else
            {
                type = 0;
            }
            Skill s = new Skill(ski.Name, type,ski.Description);

            return s;
        }
    }
}
