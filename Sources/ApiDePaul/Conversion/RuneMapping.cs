﻿using ApiDePaul.DTO;
using Model;

namespace ApiDePaul.Conversion
{
    public static class RuneMapping
    {
        public static RuneDto RuneToDto(this Rune r)
        {
            RuneDto run = new RuneDto();
            run.Name = r.Name;
            run.Description = r.Description;
            run.Icon = r.Icon;
            run.Image = r.Image.ToString();
            run.Family = r.Family.ToString();
            return run;
        }
        public static Rune DtoToRune(this RuneDto r)
        {
            RuneFamily fam;
            string[] families = { "Unknown", "Precision", "Domination" };
            if (families.ToList().Contains(r.Family))
            {
                fam = (RuneFamily)Enum.Parse(typeof(RuneFamily), r.Family);
            }
            else
            {
                fam = 0;
            }
            Rune run = new Rune(r.Name, fam);
            run.Icon = r.Icon;
            run.Image = new LargeImage(r.Image);
            run.Description = r.Description;

            return run;
        }
    }
}