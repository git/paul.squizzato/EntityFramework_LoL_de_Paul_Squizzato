﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;
using ApiDePaul.Conversion;
using ApiDePaul.DTO;
using Model;
using static System.Net.Mime.MediaTypeNames;

namespace ApiDePaul
{
    public static class ChampionMapping
    {
        public static ChampionDto ChampToDto(this Champion c)
        {
            ChampionDto champ = new ChampionDto();
            champ.Name = c.Name;
            champ.Bio = c.Bio;
            champ.Icon = c.Icon;
            champ.Image = c.Image.ToString();
            champ.Characteristics = new List<Tuple<string, int>>();
            foreach(var ch in c.Characteristics)
            {
                champ.Characteristics.Add(new Tuple<string,int>(ch.Key, ch.Value));
            }
            foreach(var sk in c.Skills)
            {
                champ.Skills.Add(sk.SkillToDto());
            }
            return champ;
        }
        public static Champion DtoToChamp(this ChampionDto c)
        {
            Champion champ = new Champion(c.Name);
            var classes = Enum.GetNames(typeof(ChampionClass));
            if (classes.ToList().Contains(c.Class))
            {
                champ.Class=(ChampionClass)Enum.Parse(typeof(ChampionClass), c.Class);
            }
            else
            {
                champ.Class = 0;
            }
            champ.Icon = c.Icon;
            champ.Image = new LargeImage(c.Image);
            champ.Bio=c.Bio;

            foreach(var s in c.Skills)
            {
                champ.AddSkill(s.DtoToSkill());
            }
            champ.AddCharacteristics(c.Characteristics.ToArray());
            /*
            //champ.Class = (ChampionClass)Enum.Parse(typeof(ChampionClass),c.Class);
            champ.Icon = c.Icon;
            //champ.Image = c.Image;
            champ.Bio = c.Bio;
            */
            return champ;
        }
    }
}