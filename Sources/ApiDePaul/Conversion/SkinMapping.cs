﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApiDePaul.DTO;
using Model;

namespace ApiDePaul
{
    public static class SkinMapping
    {
        public static SkinDto SkinToDto(this Skin s)
        {
            SkinDto ski = new SkinDto();
            ski.Name = s.Name;
            ski.Price = s.Price;
            ski.Icon = s.Icon;
            ski.Image = s.Image.ToString();
            ski.Description = s.Description;
            return ski;
        }
        //public static Skin DtoToSkin(this SkinDto s) => new Skin(s.Name);
    }
}